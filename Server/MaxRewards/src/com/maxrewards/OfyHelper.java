package com.maxrewards;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.googlecode.objectify.ObjectifyService;
import com.maxrewards.entities.objectify.Card;
import com.maxrewards.entities.objectify.Issuer;
import com.maxrewards.entities.objectify.User;

public class OfyHelper implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		ObjectifyService.register(Card.class);
		ObjectifyService.register(Issuer.class);
		ObjectifyService.register(User.class);
	}

}
