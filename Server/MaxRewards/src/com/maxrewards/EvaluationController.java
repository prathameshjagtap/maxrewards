package com.maxrewards;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maxrewards.dao.CardDAO;
import com.maxrewards.dao.UserDAO;
import com.maxrewards.entities.objectify.CardRewards;
import com.maxrewards.entities.objectify.Merchant;
import com.maxrewards.entities.objectify.UserCard;
import com.maxrewards.utility.EvaluationEngine;

@Controller
public class EvaluationController {

	@RequestMapping(value="evaluate", method=RequestMethod.POST)
	@ResponseBody
	public List<CardRewards> evaluate(@RequestParam("email") String email, 
			@RequestBody String merchantJson) {
		
		ObjectMapper mapper = new ObjectMapper();
		
		List<CardRewards> cardRewards = new ArrayList<>();
		
		try{
			Merchant merchant = mapper.readValue(merchantJson, Merchant.class);

			List<UserCard> userCards = UserDAO.getUser(email).getUserCards();
			
			EvaluationEngine engine = new EvaluationEngine();
			
			for (UserCard userCard : userCards){
				CardRewards rewards = new CardRewards();
				userCard.card = CardDAO.getCardById(userCard.issuerId, userCard.cardId);		
				rewards.card = userCard.card;

				rewards.rewards = engine.evaluatePlan(userCard.card.getPlan().get(0), merchant, userCard);
				
				cardRewards.add(rewards);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Executed: " + cardRewards.size());
		
		return cardRewards;
	}
	
}
