package com.maxrewards.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Convert {

	public static Date stringToDate(String datestr) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
		return format.parse(datestr);
	}
	
	public static Date longToDate(long date) throws ParseException {
		return new Date(date);
	}

}
