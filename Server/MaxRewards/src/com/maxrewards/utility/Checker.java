package com.maxrewards.utility;

import java.text.ParseException;
import java.util.Date;

import com.maxrewards.entities.objectify.Period;

public class Checker {

	public static boolean inCurrentPeriod(Period period) throws ParseException {
		if (period == null) return true;
		
//		String span = period.getSpan();
//		if( span != null){
//			
//			switch (span) {
//			case "quarterly":
//				
//				
//				break;
//			default:
//				break;
//			}
//			
//		}
		
		Date current = new Date();
		try {
			return 
					(period.getStartdate() == null || current.after(Convert.stringToDate(period.getStartdate()))) && 
					(period.getEnddate() == null || current.before(Convert.stringToDate(period.getEnddate())));
		} catch (ParseException e) {
			e.printStackTrace();
			throw e;
		}
	}
	
}
