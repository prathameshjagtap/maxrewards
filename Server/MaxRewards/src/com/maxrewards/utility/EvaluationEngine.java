package com.maxrewards.utility;

import java.text.ParseException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Days;

import com.maxrewards.entities.objectify.Feature;
import com.maxrewards.entities.objectify.Merchant;
import com.maxrewards.entities.objectify.Offer;
import com.maxrewards.entities.objectify.Plan;
import com.maxrewards.entities.objectify.PromotionalOffer;
import com.maxrewards.entities.objectify.Reward;
import com.maxrewards.entities.objectify.Selection;
import com.maxrewards.entities.objectify.SpecialCashback;
import com.maxrewards.entities.objectify.UpTo;
import com.maxrewards.entities.objectify.UserCard;

public class EvaluationEngine {
	
	public List<Reward> evaluatePlan(Plan plan, Merchant merchant, UserCard userCard) {
		List<Reward> rewards = null;
		try {

//			if (Checker.inCurrentPeriod(plan.getPeriod())) {
				Feature feature = plan.getFeature();
				rewards = new ArrayList<Reward>();
				
				rewards.addAll(evaluatePromotionalOffers(feature.getPromotionaloffer(), merchant, userCard, 
						plan.getDollartopoint()));
				
				List<Reward> specialRewards = evaluateSpecialCashback(feature.getSpecialcashback(), merchant, userCard);
				
				if (specialRewards.size() > 0) {
					rewards.addAll(specialRewards);
				} else {
					Reward other = feature.getOther();
					other.setOfferType(Reward.OTHER);
					rewards.add(other);
				}
//			}
			return rewards;
		} catch (ParseException e) {
			e.printStackTrace();
			return rewards;
		}
	}

	/**
	 * Evaluates if the promotional offers are still running, and if they are, what will be benifit value.
	 * @param promotionalOffers
	 * @param merchant
	 * @param userCard
	 * @return
	 * @throws ParseException
	 */
	public List<Reward> evaluatePromotionalOffers(PromotionalOffer[] promotionalOffers, 
			Merchant merchant, UserCard userCard, double dollartopoint) throws ParseException{
		if (promotionalOffers == null) return new ArrayList<Reward>();
		
		List<Reward> rewards = new ArrayList<Reward>();
		for (PromotionalOffer offer : promotionalOffers) {

			UpTo limit = offer.getUpto();

			if (isInUpToLimit(limit, userCard) && userCard.considerPromo) {
				Reward reward = new Reward();
				reward.setOfferType(Reward.PROMOTIONAL);
				Reward promoReward = offer.getReward();
				promoReward.setOfferType(Reward.PROMOTIONAL);

				switch (promoReward.getUnit()) {
				case "flat":
					double x = (promoReward.getValue()/limit.getAmount()) * 100;
					reward.setUnit("%");
					reward.setValue(x);
					break;
				case "flatpoint":
					//TODO Add code to convert Amount to points
					x = (promoReward.getValue()/(limit.getAmount()*dollartopoint)) * 100;
					reward.setUnit("p");
					reward.setValue(x);
					break;
				case "match":
					//TODO 
					break;
				case "%":
					reward = promoReward;
					break;
				}

				rewards.add(reward);
			}

		}
		return rewards;
	}

	/**
	 * This function returns if the promotional is still valid with the provided user card details.
	 * If we don't have user card details, basically we cannot evaluate the promotions.
	 * 
	 * @param upto
	 * @param userCard
	 * @return
	 * @throws ParseException
	 */
	public boolean isInUpToLimit(UpTo upto, UserCard userCard) throws ParseException {

		// if there is no limit
		if (upto == null) return true;

		boolean uptoFlag = true;

		// No enough information to do promotional offer check
		if (userCard.balance == -1 && userCard.startDate == -1)
			return false;

		// If we have any days info for card and startdate of user's card
		if (upto.getDays() != -1 && userCard.startDate != -1) {
			Days days = Days.daysBetween(new DateTime(new Date()), 
					new DateTime(Convert.longToDate(userCard.startDate)));
			if (upto.getDays() < Math.abs( days.getDays())) {
				uptoFlag = false;
			}
		}

		// If we have any maximum amount info for card and balance of user's card
		if (upto.getAmount() != -1 && userCard.balance != -1) {
			if (upto.getAmount() < userCard.balance) {
				uptoFlag = false;
			}
		}

		return uptoFlag;
	}

	public List<Reward> evaluateSpecialCashback(SpecialCashback specialCashback, 
			Merchant merchant, UserCard userCard) throws ParseException{
		
		if (specialCashback == null)  return new ArrayList<Reward>();
		
		if (	// if it is in current period
				!Checker.inCurrentPeriod(specialCashback.getPeriod())
				// if the limits are crossed and 
				&& !isInUpToLimit(specialCashback.getUpto(), userCard) 
				// If user opt's out of considering limits
				&& !userCard.considerSpecial
				)
			return new ArrayList<Reward>();

		List<Reward> rewards = new ArrayList<Reward>();
		for (Offer offer : specialCashback.getOffers()) {

			if (!Checker.inCurrentPeriod(offer.getPeriod())) continue;

			UpTo upto = offer.getUpto();

			if (isInUpToLimit(upto, userCard)) {

				if (evaluateSelections(offer.getSelection(), merchant)){

					Reward reward = new Reward();
					reward.setOfferType(Reward.SPECIAL);
					Reward promoReward = offer.getReward();
					promoReward.setOfferType(Reward.SPECIAL);

					switch (promoReward.getUnit()) {
					case "flat":
						double x = (reward.getValue()/upto.getAmount()) * 100;
						reward.setUnit("%");
						reward.setValue(x);
						break;
					case "%":
						reward = promoReward;
						break;
					}

					rewards.add(reward);
				}
			}

		}
		return rewards;
	}

	private boolean evaluateSelections(Selection[] selections, Merchant merchant) {
		
		for (Selection selection : selections) {
			
			switch (selection.getType()) {
			case "category":
				for (String category : merchant.types) {
					if (category.equals(selection.getValue())) {
						return true;
					}
				}
				break;
			case "merchant":
				if (merchant.name.equalsIgnoreCase(selection.getValue()))
					return true;
				break;
			}
			
		}
		
		return false;
	}

}
