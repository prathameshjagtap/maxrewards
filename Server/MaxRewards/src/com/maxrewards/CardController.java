package com.maxrewards;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.maxrewards.dao.CardDAO;
import com.maxrewards.entities.objectify.Card;
import com.maxrewards.entities.objectify.Issuer;

@Controller
public class CardController {

	@RequestMapping(value="card", method=RequestMethod.POST)
	@ResponseBody
	public Card addCard(@RequestParam("cardJson") String cardJson){
		
		ObjectMapper mapper = new ObjectMapper();
		
		Card card = null;
		try {
			card = mapper.readValue(cardJson, Card.class);
			
			ofy().save().entity(card).now();
			
			
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		return card;
	}
	
	@RequestMapping(value="card/{issuerId}", method=RequestMethod.GET)
	@ResponseBody
	public List<Card> cards(@PathVariable("issuerId") String issuerId){
		
		Key<Issuer> issuer = Key.create(Issuer.class, issuerId);
		
		List<Card> cards = ObjectifyService.ofy()
		          .load()
		          .type(Card.class) // We want only Greetings
		          .ancestor(issuer)    // Anyone in this book
		          .list();
		
		return cards;
	}
	
	@RequestMapping(value="card", method=RequestMethod.GET)
	@ResponseBody
	public List<Card> cards(){
		
		List<Card> cards = ObjectifyService.ofy()
		          .load()
		          .type(Card.class) // We want only Greetings
		          .list();
		
		return cards;
	}

	@RequestMapping(value="card/{issuerId}/{cardid}", method=RequestMethod.GET)
	@ResponseBody
	public Card card(@PathVariable("issuerId") String issuerId, @PathVariable("cardid") String cardid){
		return CardDAO.getCardById(issuerId, cardid);
	}


//	@RequestMapping(value="card", method=RequestMethod.POST)
//	@ResponseBody
//	public Card addCard(@RequestParam("cardJson") String cardJson){
//		
//		ObjectMapper mapper = new ObjectMapper();
//		
//		Card card = null;
//		try {
//			card = mapper.readValue(cardJson, Card.class);
//			
//			PersistenceManager factory = PMF.get().getPersistenceManager();
//			factory.makePersistent(card);
//			factory.close();
//			
//		}catch (IOException e) {
//			e.printStackTrace();
//		}
//		
//		return card;
//	}

//	@RequestMapping(value="card", method=RequestMethod.GET)
//	@ResponseBody
//	public List<Card> cards(){
//		
//		PersistenceManager pm = PMF.get().getPersistenceManager();
//		Query query = pm.newQuery("SELECT FROM " + Card.class.getName());
//		List<Card> cards = (List<Card>)query.execute();
//		pm.close();
//		
//		return cards;
//	}
//	
//	@RequestMapping(value="card/{cardid}", method=RequestMethod.GET)
//	@ResponseBody
//	public Card card(@PathVariable("cardid") String cardid){
//		
//		PersistenceManager pm = PMF.get().getPersistenceManager();
//		Query query = pm.newQuery(Card.class);
//		query.setFilter("id == idparam");
//		query.declareParameters("String idparam");
//		List<Card> cards = (List<Card>)query.execute(cardid);
//		pm.close();
//		
//		return cards.size() > 0 ? cards.get(0) : null;
//	}
	
}
