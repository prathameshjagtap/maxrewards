package com.maxrewards.dao;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.maxrewards.entities.objectify.Card;
import com.maxrewards.entities.objectify.Issuer;

public class CardDAO {
	
	public static Card getCardById(String issuerId, String cardid){
		
		Key<Issuer> issuer = Key.create(Issuer.class, issuerId);
		
		Card card = ObjectifyService.ofy()
		          .load()
		          .key(Key.create(issuer, Card.class, cardid))
		          .now();
		
		return card;
	}
	
}
