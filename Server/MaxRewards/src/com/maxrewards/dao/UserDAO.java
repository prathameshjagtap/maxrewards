package com.maxrewards.dao;

import com.googlecode.objectify.ObjectifyService;
import com.maxrewards.entities.objectify.User;

public class UserDAO {

	public static User getUser(String email) {
		User user = ObjectifyService.ofy()
		          .load()
		          .type(User.class)
		          .id(email)
		          .now();
		
		return user;
	}
	
}
