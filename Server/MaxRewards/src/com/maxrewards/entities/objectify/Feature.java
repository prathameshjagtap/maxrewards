package com.maxrewards.entities.objectify;

public class Feature {
	
	PromotionalOffer[] promotionaloffer;

	SpecialCashback specialcashback;

	Benefit[] benefit;
	
	Reward other;

	String message;
	
	public Benefit[] getBenefit() {
		return benefit;
	}
	public void setBenefit(Benefit[] benefit) {
		this.benefit = benefit;
	}
	public PromotionalOffer[] getPromotionaloffer() {
		return promotionaloffer;
	}
	public void setPromotionaloffer(PromotionalOffer[] promotionaloffer) {
		this.promotionaloffer = promotionaloffer;
	}
	public SpecialCashback getSpecialcashback() {
		return specialcashback;
	}
	public void setSpecialcashback(SpecialCashback specialcashback) {
		this.specialcashback = specialcashback;
	}
	public Reward getOther() {
		return other;
	}
	public void setOther(Reward other) {
		this.other = other;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
