package com.maxrewards.entities.objectify;

public class Plan {
	
	Period period;
	
	Feature feature;
	
	double dollartopoint = -1;
	
	public Period getPeriod() {
		return period;
	}
	public void setPeriod(Period period) {
		this.period = period;
	}
	public Feature getFeature() {
		return feature;
	}
	public void setFeature(Feature feature) {
		this.feature = feature;
	}
	public double getDollartopoint() {
		return dollartopoint;
	}
	public void setDollartopoint(double dollartopoint) {
		this.dollartopoint = dollartopoint;
	}
	
}
