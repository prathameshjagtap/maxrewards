package com.maxrewards.entities.objectify;

import java.util.List;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Parent;

@Entity
public class Card {
	@Parent Key<Issuer> issuerKey;
	
	@Id
	String id;
	
	String cardname;
	
	String img;
	
	String issuer;
	
	String promotionalMessage;
	String specialMessage;
	String otherMessage;
	
	List<Plan> plan;

	public String getImg() {
		return img;
	}
	
	public void setImg(String img) {
		this.img = img;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getCardname() {
		return cardname;
	}

	public void setCardname(String cardname) {
		this.cardname = cardname;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
		
		if (issuer != null) {
			issuerKey = Key.create(Issuer.class, issuer);
		} else {
			issuerKey = Key.create(Issuer.class, "unknown");
		}
	}
	
	public List<Plan> getPlan() {
		return plan;
	}
	
	public void setPlan(List<Plan> plan) {
		this.plan = plan;
	}

	public String getPromotionalMessage() {
		return promotionalMessage;
	}

	public void setPromotionalMessage(String promotionalMessage) {
		this.promotionalMessage = promotionalMessage;
	}

	public String getSpecialMessage() {
		return specialMessage;
	}

	public void setSpecialMessage(String specialMessage) {
		this.specialMessage = specialMessage;
	}

	public String getOtherMessage() {
		return otherMessage;
	}

	public void setOtherMessage(String otherMessage) {
		this.otherMessage = otherMessage;
	}
	
}
