package com.maxrewards.entities.objectify;

public class Offer {
	
	Selection[] selection;
	
	Reward reward;
	
	Period period;

	UpTo upto;
	
	public UpTo getUpto() {
		return upto;
	}
	public void setUpto(UpTo upto) {
		this.upto = upto;
	}
	public Period getPeriod() {
		return period;
	}
	public void setPeriod(Period period) {
		this.period = period;
	}
	public Selection[] getSelection() {
		return selection;
	}
	public void setSelection(Selection[] selection) {
		this.selection = selection;
	}
	public Reward getReward() {
		return reward;
	}
	public void setReward(Reward reward) {
		this.reward = reward;
	}
	
	
}
