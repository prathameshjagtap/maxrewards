package com.maxrewards.entities.objectify;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class Issuer {
	
	@Id String id;
	
	String name;
	
	String description;
	
}
