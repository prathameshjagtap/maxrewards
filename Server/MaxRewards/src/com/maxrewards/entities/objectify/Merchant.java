package com.maxrewards.entities.objectify;

import java.util.ArrayList;
import java.util.List;

public class Merchant {
	public String name, streetaddress;
	
	public List<String> types;
	
	public Merchant() {
		types = new ArrayList<String>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStreetaddress() {
		return streetaddress;
	}

	public void setStreetaddress(String streetaddress) {
		this.streetaddress = streetaddress;
	}

	public List<String> getTypes() {
		return types;
	}

	public void setTypes(List<String> types) {
		this.types = types;
	}
	
}
