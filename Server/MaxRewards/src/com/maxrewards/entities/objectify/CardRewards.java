package com.maxrewards.entities.objectify;

import java.util.List;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Parent;

public class CardRewards {
	
	public Card card;
	public List<Reward> rewards;
	
}
