package com.maxrewards.entities.objectify;

public class SpecialCashback {
	
	Period period;

	UpTo upto;
	
	Offer[] offers;
	
	String message;
	public Period getPeriod() {
		return period;
	}
	public void setPeriod(Period period) {
		this.period = period;
	}
	public UpTo getUpto() {
		return upto;
	}
	public void setUpto(UpTo upto) {
		this.upto = upto;
	}
	public Offer[] getOffers() {
		return offers;
	}
	public void setOffers(Offer[] offers) {
		this.offers = offers;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
