package com.maxrewards.entities.objectify;

import com.google.appengine.api.datastore.Key;

public class Benefit {
	
    private Key key;
	
	String title;

	String descrpition;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescrpition() {
		return descrpition;
	}
	public void setDescrpition(String descrpition) {
		this.descrpition = descrpition;
	}
	
}
