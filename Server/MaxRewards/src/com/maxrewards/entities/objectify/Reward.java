package com.maxrewards.entities.objectify;

public class Reward {
	
	public static final byte PROMOTIONAL = 1;
	public static final byte SPECIAL = 2;
	public static final byte OTHER = 3;
	
	byte offerType;
	
	String unit;

	double value;
	
	String type;
	
	public byte getOfferType() {
		return offerType;
	}
	public void setOfferType(byte offerType) {
		this.offerType = offerType;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "Reward [offerType=" + offerType + ", unit=" + unit + ", value=" + value + ", type=" + type + "]";
	}
	
}
