package com.maxrewards.entities.objectify;

public class UserCard {

	public long startDate;
	public int balance;
	public int limit;
	public boolean considerPromo;
	public boolean considerSpecial;
	
	public String issuerId;
	public String cardId;
	
	public Card card;
	
}
