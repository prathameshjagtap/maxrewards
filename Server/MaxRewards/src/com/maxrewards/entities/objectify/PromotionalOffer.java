package com.maxrewards.entities.objectify;

public class PromotionalOffer {
	
	UpTo upto;
	
	Reward reward;
	
	public UpTo getUpto() {
		return upto;
	}
	public void setUpto(UpTo upto) {
		this.upto = upto;
	}
	public Reward getReward() {
		return reward;
	}
	public void setReward(Reward reward) {
		this.reward = reward;
	}
	
}
