package com.maxrewards.entities.objectify;

public class UpTo {
	
	double amount = -1;

	int days = -1;
	
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public int getDays() {
		return days;
	}
	public void setDays(int days) {
		this.days = days;
	}
	
}
