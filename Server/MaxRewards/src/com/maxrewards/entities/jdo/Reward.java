package com.maxrewards.entities.jdo;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable
public class Reward {
	
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Key key;
	
	public static final byte PROMOTIONAL = 1;
	public static final byte SPECIAL = 2;
	public static final byte OTHER = 3;
	
	@Persistent
	byte offerType;
	
	@Persistent
	String unit;

	@Persistent
	double value;
	
	@Persistent
	String type;
	
	public byte getOfferType() {
		return offerType;
	}
	public void setOfferType(byte offerType) {
		this.offerType = offerType;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "Reward [offerType=" + offerType + ", unit=" + unit + ", value=" + value + ", type=" + type + "]";
	}
	
}
