package com.maxrewards.entities.jdo;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable
public class Offer {
	
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Key key;
	
	@Persistent(defaultFetchGroup = "true") 
	Selection[] selection;
	
	@Persistent(defaultFetchGroup = "true") 
	Reward reward;
	
	@Persistent(defaultFetchGroup = "true") 
	Period period;

	@Persistent(defaultFetchGroup = "true") 
	UpTo upto;
	
	public UpTo getUpto() {
		return upto;
	}
	public void setUpto(UpTo upto) {
		this.upto = upto;
	}
	public Period getPeriod() {
		return period;
	}
	public void setPeriod(Period period) {
		this.period = period;
	}
	public Selection[] getSelection() {
		return selection;
	}
	public void setSelection(Selection[] selection) {
		this.selection = selection;
	}
	public Reward getReward() {
		return reward;
	}
	public void setReward(Reward reward) {
		this.reward = reward;
	}
	
	
}
