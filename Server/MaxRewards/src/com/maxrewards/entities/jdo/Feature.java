package com.maxrewards.entities.jdo;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable
public class Feature {
	
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Key key;
	
	@Persistent(defaultFetchGroup = "true") 
	PromotionalOffer[] promotionaloffer;

	@Persistent(defaultFetchGroup = "true") 
	SpecialCashback specialcashback;

	@Persistent(defaultFetchGroup = "true") 
	Benefit[] benefit;
	
	@Persistent(defaultFetchGroup = "true") 
	Reward other;

	@Persistent
	String message;
	
	public Benefit[] getBenefit() {
		return benefit;
	}
	public void setBenefit(Benefit[] benefit) {
		this.benefit = benefit;
	}
	public PromotionalOffer[] getPromotionaloffer() {
		return promotionaloffer;
	}
	public void setPromotionaloffer(PromotionalOffer[] promotionaloffer) {
		this.promotionaloffer = promotionaloffer;
	}
	public SpecialCashback getSpecialcashback() {
		return specialcashback;
	}
	public void setSpecialcashback(SpecialCashback specialcashback) {
		this.specialcashback = specialcashback;
	}
	public Reward getOther() {
		return other;
	}
	public void setOther(Reward other) {
		this.other = other;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
