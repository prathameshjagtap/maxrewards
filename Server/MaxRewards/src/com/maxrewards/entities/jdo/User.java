package com.maxrewards.entities.jdo;

import java.util.ArrayList;
import java.util.List;

public class User {

	String name;
	
	String email;
	String password;
	String accessToken;
	String refreshToken;
	
	List<UserCard> userCards; 
	
	public User() {
		userCards = new ArrayList<>();
	}
}
