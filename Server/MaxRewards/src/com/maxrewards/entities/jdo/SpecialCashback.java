package com.maxrewards.entities.jdo;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable
public class SpecialCashback {
	
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Key key;
	
	@Persistent(defaultFetchGroup = "true") 
	Period period;

	@Persistent(defaultFetchGroup = "true") 
	UpTo upto;
	
	@Persistent(defaultFetchGroup = "true") 
	Offer[] offers;
	
	@Persistent
	String message;
	public Period getPeriod() {
		return period;
	}
	public void setPeriod(Period period) {
		this.period = period;
	}
	public UpTo getUpto() {
		return upto;
	}
	public void setUpto(UpTo upto) {
		this.upto = upto;
	}
	public Offer[] getOffers() {
		return offers;
	}
	public void setOffers(Offer[] offers) {
		this.offers = offers;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
