package com.maxrewards.entities.jdo;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable
public class PromotionalOffer {
	
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Key key;
	
	@Persistent(defaultFetchGroup = "true") 
	UpTo upto;
	
	@Persistent(defaultFetchGroup = "true") 
	Reward reward;
	
	public UpTo getUpto() {
		return upto;
	}
	public void setUpto(UpTo upto) {
		this.upto = upto;
	}
	public Reward getReward() {
		return reward;
	}
	public void setReward(Reward reward) {
		this.reward = reward;
	}
	
}
