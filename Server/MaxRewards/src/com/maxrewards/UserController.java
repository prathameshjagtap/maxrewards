package com.maxrewards;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maxrewards.dao.CardDAO;
import com.maxrewards.dao.UserDAO;
import com.maxrewards.entities.objectify.User;
import com.maxrewards.entities.objectify.UserCard;

@Controller
public class UserController {

	@RequestMapping(value="user", method=RequestMethod.POST)
	@ResponseBody
	public User addUser(@RequestBody String userJson){
		
		ObjectMapper mapper = new ObjectMapper();
		
		User user = null;
		try {
			user = mapper.readValue(userJson, User.class);
			
			ofy().save().entity(user).now();			
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		return user;
	}
	
	@RequestMapping(value="authuser", method=RequestMethod.POST)
	@ResponseBody
	public User authUser(@RequestBody String userJson){
		
		
		ObjectMapper mapper = new ObjectMapper();
		
		User user = null;
		try {
			user = mapper.readValue(userJson, User.class);

			User oguser = UserDAO.getUser(user.getEmail());
			
			if (oguser == null || !oguser.getPassword().equals(user.getPassword()))
				user = null;
			
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		return user;
	}
	
	@RequestMapping(value="user", method=RequestMethod.GET)
	@ResponseBody
	public User user(@RequestParam("email") String email){
		return UserDAO.getUser(email);
	}
	
	@RequestMapping(value="user/cards", method=RequestMethod.GET)
	@ResponseBody
	public List<UserCard> getCards(@RequestParam("email") String email){
		
		List<UserCard> userCards = UserDAO.getUser(email).getUserCards();
		
		for (UserCard userCard : userCards) {
			userCard.card = CardDAO.getCardById(userCard.issuerId, userCard.cardId);
		}

		return userCards;
	}

	@RequestMapping(value="user/card", method=RequestMethod.GET)
	@ResponseBody
	public UserCard getCard(@RequestParam("email") String email, 
			@RequestParam("userCardJson") String userCardJson){
		
		User user = UserDAO.getUser(email);
		
		ObjectMapper mapper = new ObjectMapper();
		UserCard userCard = null;
		try {
			userCard = mapper.readValue(userCardJson, UserCard.class);
			
			List<UserCard> userCards = user.getUserCards();
			
			for (int i =0 ; i < userCards.size() ; i ++) {
				if (userCards.get(i).cardId.equalsIgnoreCase(userCard.cardId)) {
					userCard  = userCards.get(i);
				}
			}
			
			userCard.card = CardDAO.getCardById(userCard.issuerId, userCard.cardId);
			
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		return userCard;
	}
	
	@RequestMapping(value="user/card", method=RequestMethod.POST)
	@ResponseBody
	public UserCard addCard(@RequestParam("email") String email, 
			@RequestBody String userCardJson, HttpServletResponse response){
		
		User user = UserDAO.getUser(email);
		
		ObjectMapper mapper = new ObjectMapper();
		UserCard userCard = null;
		try {
			userCard = mapper.readValue(userCardJson, UserCard.class);
			
			user.getUserCards().add(userCard);
			ofy().save().entity(user).now();
			
			userCard.card = CardDAO.getCardById(userCard.issuerId, userCard.cardId);
			return userCard;
			
		}catch (IOException e) {
			e.printStackTrace();
			try {
				response.sendError(500, e.getMessage());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			return null;
		}
		
	}
	
	@RequestMapping(value="user/card", method=RequestMethod.PUT)
	@ResponseBody
	public User updateCard(@RequestParam("email") String email, 
			@RequestParam("userCardJson") String userCardJson){
		
		User user = UserDAO.getUser(email);
		
		ObjectMapper mapper = new ObjectMapper();
		UserCard userCard;
		try {
			userCard = mapper.readValue(userCardJson, UserCard.class);
			
			List<UserCard> userCards = user.getUserCards();
			
			for (int i =0 ; i < userCards.size() ; i ++) {
				if (userCards.get(i).cardId.equalsIgnoreCase(userCard.cardId)) {
					userCards.set(i, userCard);
				}
			}
			
			ofy().save().entity(user).now();
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		return user;
	}
	
	@RequestMapping(value="user/card", method=RequestMethod.DELETE)
	@ResponseBody
	public User deleteCard(@RequestParam("email") String email, 
			@RequestParam("userCardJson") String userCardJson){
		
		User user = UserDAO.getUser(email);
		
		ObjectMapper mapper = new ObjectMapper();
		UserCard userCard;
		try {
			userCard = mapper.readValue(userCardJson, UserCard.class);
			
			List<UserCard> userCards = user.getUserCards();
			
			for (int i =0 ; i < userCards.size() ; i ++) {
				if (userCards.get(i).cardId.equalsIgnoreCase(userCard.cardId)) {
					userCards.remove(i);
				}
			}
			
			ofy().save().entity(user).now();
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		return user;
	}
	
}
