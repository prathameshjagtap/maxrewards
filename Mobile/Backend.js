

var getBaseURL = function(){
	return "http://1-dot-maxrewards-152602.appspot.com";
//	return "http://localhost:8888";
}

var Issuer = function(id,name,logo){
	this.id = id;
	this.name = name;
	this.logo = logo;
}

function fetchIssuers(callback){
	setTimeout(function(){
		callback([
			new Issuer('Amex','American Express','amexlogo'),
			new Issuer('Bofa','Bank of America','bofalogo'),
			new Issuer('Barclay','Barclay\'s','barclaylogo'),
			new Issuer('Capital One','Capital One','caponelogo'),
			new Issuer('Chase','Chase','chaselogo'),
			new Issuer('Citi','Citi','citilogo'),
			new Issuer('Discover','Discover','discoverlogo')
			]);
	},0)
}

var UserCard = function(cardId,issuerId,startDate,balance,considerPromo,considerSpecial){
	this.cardId = cardId;
	this.issuerId = issuerId;
	this.startDate = startDate;
	this.balance = balance;
	this.considerPromo = considerPromo;
	this.considerSpecial = considerSpecial;
}

function fetchCards(issuerid, callback){

	var status = 0;
	var response_ok = false;

	fetch(getBaseURL() + "/card/" + issuerid , {
		method: 'GET',
		headers: { "Content-type": "application/json"}
	}).then(function(response) {
    	status = response.status;  // Get the HTTP status code
    	response_ok = response.ok; // Is response.status in the 200-range?
    	return response.json();    // This returns a promise
	}).then(function(responseObject) {
    	callback(responseObject);
	}).catch(function(err) {
		console.log("Error:" + JSON.stringify(err));
		callback(null)
	});
}

function fetchCard(cardid, issuerid, callback){

	var status = 0;
	var response_ok = false;

	fetch(getBaseURL() + "/card/" + issuerid + "/" + cardid, {
		method: 'GET',
		headers: { "Content-type": "application/json"}
	}).then(function(response) {
    	status = response.status;  // Get the HTTP status code
    	response_ok = response.ok; // Is response.status in the 200-range?
    	return response.json();    // This returns a promise
	}).then(function(responseObject) {
    	callback(responseObject);
	}).catch(function(err) {
		console.log("Error:" + JSON.stringify(err));
		callback(null)
	});
}

function addToAccount(card, callback){
	var myCard = new UserCard(card.id,card.issuer,new Date().getTime(),0,true,true);

	fetch(getBaseURL() + "/user/card?email=" + getUser().email, {
		method: 'POST',
		headers: { "Content-type": "application/json"},
		body: JSON.stringify(myCard)
	}).then(function(response) {
    	status = response.status;  // Get the HTTP status code
    	response_ok = response.ok; // Is response.status in the 200-range?
    	return response.json();    // This returns a promise
	}).then(function(responseObject) {
    	callback(responseObject);
	}).catch(function(err) {
		console.log("Error:" + JSON.stringify(err));
		callback(null)
	});
}

function updateCard(myCard, callback){
	fetch(getBaseURL() + "/user/card?email=" + getUser().email + "&userCardJson=" + JSON.stringify(myCard) , {
		method: 'PUT',
		headers: { "Content-type": "application/json"}
	}).then(function(response) {
    	status = response.status;  // Get the HTTP status code
    	response_ok = response.ok; // Is response.status in the 200-range?
    	return response.json();    // This returns a promise
	}).then(function(responseObject) {
    	callback(responseObject);
	}).catch(function(err) {
		console.log("Error:" + JSON.stringify(err));
		callback(null)
	});
}

function removeCard(myCard, callback){
	fetch(getBaseURL() + "/user/card?email=" + getUser().email + "&userCardJson=" + JSON.stringify(myCard) , {
		method: 'DELETE',
		headers: { "Content-type": "application/json"}
	}).then(function(response) {
    	status = response.status;  // Get the HTTP status code
    	response_ok = response.ok; // Is response.status in the 200-range?
    	return response.json();    // This returns a promise
	}).then(function(responseObject) {
    	callback();
	}).catch(function(err) {
		console.log("Error:" + JSON.stringify(err));
		callback()
	});
}

var fetchMyCard = function(myCard, callback){
	fetch(getBaseURL() + "/user/card?email=" + getUser().email + "&userCardJson=" + JSON.stringify(myCard) , {
		method: 'GET',
		headers: { "Content-type": "application/json"}
	}).then(function(response) {
    	status = response.status;  // Get the HTTP status code
    	response_ok = response.ok; // Is response.status in the 200-range?
    	return response.json();    // This returns a promise
	}).then(function(myCard) {
		fetchCard(myCard.cardId, myCard.issuerId, function(newMyCard){
			newMyCard.img = getBaseURL() + newMyCard.img;
			myCard.card = newMyCard;
	    	callback(myCard);
		});
	}).catch(function(err) {
		console.log("Error:" + JSON.stringify(err));
		callback(null)
	});

}

var fetchMyCards = function(callback){
	fetch(getBaseURL() + "/user/cards?email=" + getUser().email , {
		method: 'GET',
		headers: { "Content-type": "application/json"}
	}).then(function(response) {
    	status = response.status;  // Get the HTTP status code
    	response_ok = response.ok; // Is response.status in the 200-range?
    	return response.json();    // This returns a promise
	}).then(function(myCards) {
		myCards.forEach(function(myCard){
			myCard.card.img = getBaseURL() + myCard.card.img;
		});
		callback(myCards);
	}).catch(function(err) {
		console.log("Error:" + JSON.stringify(err));
		callback(null)
	});
};

var user = null;

var login = function(email, password, callback){
	user = {
		email : email,
		password: password
	};

	fetch(getBaseURL() + "/authuser" , {
		method: 'POST',
		headers: { "Content-type": "application/json"},
		body: JSON.stringify(user)
	}).then(function(response) {
    	status = response.status;  // Get the HTTP status code
    	response_ok = response.ok; // Is response.status in the 200-range?
    	return response.json();    // This returns a promise
	}).then(function(response) {
		callback(response);
	}).catch(function(err) {
		console.log("Error:" + JSON.stringify(err));
		callback(null)
	});
}

var setUser = function(newuser, callback){
	user = newuser;
	callback();
}

var signup = function(email, password, callback){
	user = {
		email : email,
		password: password
	};

	fetch(getBaseURL() + "/user"  , {
		method: 'POST',
		headers: { "Content-type": "application/json"},
		body: JSON.stringify(user)
	}).then(function(response) {
    	status = response.status;  // Get the HTTP status code
    	response_ok = response.ok; // Is response.status in the 200-range?
    	return response.json();    // This returns a promise
	}).then(function(response) {
		callback(response);
	}).catch(function(err) {
		console.log("Error:" + JSON.stringify(err));
		callback(null)
	});
}

var getUser = function(callback){
	return user;
}

var evaluate = function(merchant, callback){
	var newmerchant = {
		name: merchant.name,
		streetaddress: merchant.streetaddress,
		types: merchant.types
	};

	fetch(getBaseURL() + "/evaluate?email=" + getUser().email, {
		method: 'POST',
		headers: { "Content-type": "application/json"},
		body: JSON.stringify(newmerchant)
	}).then(function(response) {
    	status = response.status;  // Get the HTTP status code
    	response_ok = response.ok; // Is response.status in the 200-range?
    	return response.json();    // This returns a promise
	}).then(function(cardRewards) {
		callback(cardRewards);
	}).catch(function(err) {
		console.log("Error:" + JSON.stringify(err));
	});

}

var processImageURL = function(img){
	return (getBaseURL() + img);
}

module.exports = {
	Issuer: Issuer,
	fetchIssuers: fetchIssuers,
	fetchCards: fetchCards,
	addToAccount: addToAccount,
	updateCard:updateCard,
	removeCard:removeCard,
	fetchMyCard: fetchMyCard,
	fetchMyCards: fetchMyCards,
	getBaseURL: getBaseURL,
	login: login,
	signup: signup,
	setUser: setUser,
	getUser: getUser,
	evaluate:evaluate,
	processImageURL: processImageURL
};